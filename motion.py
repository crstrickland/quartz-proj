import numpy as np
import cv2 as cv
import argparse
from statistics import harmonic_mean
from collections import deque
import sqlite3
from time import time

# Params from the opencv examples
feature_params = dict( maxCorners = 100,
                       qualityLevel = 0.3,
                       minDistance = 7,
                       blockSize = 7 )
lk_params = dict( winSize  = (15,15),
                  maxLevel = 2,
                  criteria = (cv.TERM_CRITERIA_EPS | cv.TERM_CRITERIA_COUNT, 10, 0.03))

color = np.random.randint(0,255,(100,3))


def get_motion_chunks(video, track_points=True, display=False):

    cap = cv.VideoCapture(video)

    # Get first frame, find features
    ret, old_frame = cap.read()
    old_gray = cv.cvtColor(old_frame, cv.COLOR_BGR2GRAY)
    p0 = cv.goodFeaturesToTrack(old_gray, mask = None, **feature_params)
    
    # Set up our displacement and motion buffers
    persist_max = 60
    prev_hmean_disps = deque([.5], maxlen = persist_max)
    motions = deque(maxlen = 2)
    motion_history = []

    fourcc = cv.VideoWriter_fourcc(*'XVID')
    vout = cv.VideoWriter('out.avi', fourcc, 30, (1920, 1080))

    if display:
        cv.namedWindow('frame', cv.WINDOW_NORMAL)
        cv.resizeWindow('frame', 1920, 1080)

    while(1):
        ret,frame = cap.read()

        if ret == False:
            break

        frame_gray = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)

        # calculate optical flow
        p1, st, err = cv.calcOpticalFlowPyrLK(old_gray, frame_gray, p0, None, **lk_params)
        good_new = p1[st==1]
        good_old = p0[st==1]

        displacements = []
        
        # draw the tracks
        for i,(new,old) in enumerate(zip(good_new, good_old)):
            a,b = new.ravel()
            c,d = old.ravel()
            if track_points: 
                frame = cv.circle(frame,(a,b),10,color[i].tolist(),-1)
            displacements.append(np.linalg.norm((a - c,b - d)))

        hmean_disp = harmonic_mean(displacements)
        motion = True if hmean_disp > max(1, min(2 * harmonic_mean(list(prev_hmean_disps)), 5)) else False 
        prev_hmean_disps.append(hmean_disp)
       
        # our history consists of tuples of (state, start_msec)
        # when we detect consistent movement conditions over a few frames, note the time
        # the bias in time (determined by our frame look-back) can be fixed by using frame positions
        # instead of direct frame times, but is not that important for looking at  the proportion
        # of time spent in each state
        if all([m is motion for m in motions]):
            motion_history.append((motion, cap.get(cv.CAP_PROP_POS_MSEC)))
        motions.append(motion)

        img = frame.copy()
        
        cv.putText(
            img,
            "MOVING" if motion_history[-1][0] else "STOPPED",
            (50, 100),
            cv.FONT_HERSHEY_SIMPLEX,
            4,
            (0, 255, 0) if motion_history[-1][0] else (0, 0, 255),
            8
        )
        
        vout.write(cv.resize(img, (1920, 1080)))

        if display:
            cv.imshow('frame',img)
            k = cv.waitKey(3) & 0xff
            if k == 27:
                break

        # Now update the previous frame and previous points
        old_gray = frame_gray.copy()
        p0 = good_new.reshape(-1,1,2)

    cap.release()
    vout.release()
    cv.destroyAllWindows()

    return motion_history


def merge_chunks(history):
    if not history:
        return []

    new_hist = []
    cur_state = history[0][0]
    start_time = history[0][1]
    end_time = history[0][1]

    for h in history[1:]:
        if h[0] != cur_state:
            new_hist.append((cur_state, start_time, h[1]))
            cur_state = h[0]
            start_time = h[1]

    return new_hist


def insert_into_db(chunks, db_name='test.db'):
    conn = sqlite3.connect(db_name)
    c = conn.cursor()
    table_name = 'chunks_' + str(int(time()))
    c.execute(f'''CREATE TABLE {table_name} (timestamp text, motion integer, start real, stop real)''')
    fchunks = [(str(int(time())), 1 if c[0] else 0, c[1], c[2]) for c in chunks]
    c.executemany(f'''INSERT INTO {table_name} VALUES (?, ?, ?, ?)''', fchunks)
    conn.commit()
    conn.close()

    return table_name


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('image', type=str, help='path to image file')
    args = parser.parse_args()

    chunks = get_motion_chunks(args.image, True, True)

    merged = merge_chunks(chunks)
    print('motion periods:')
    print('\n'.join([str(m) for m in merged]))
