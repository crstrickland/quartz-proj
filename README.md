python motion.py [video] will print a list of intervals and their motion state

Or, pip install -r requirements.txt and run pytest to do a test against sqlite

Either way will generate out.avi, containing a video with motion state.