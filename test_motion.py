import motion
import sqlite3

# the statistics package will throw warnings
def test_short():
    chunks = motion.get_motion_chunks('short.mov', False, False)
    merged = motion.merge_chunks(chunks)
    table = motion.insert_into_db(merged, 'pytest.db')

    conn = sqlite3.connect('pytest.db')
    c = conn.cursor()
    c.execute(f'SELECT * from {table}')
    rows = c.fetchall()
    conn.close()

    assert len(rows) == 16
